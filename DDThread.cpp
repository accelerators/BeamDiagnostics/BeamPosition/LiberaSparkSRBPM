//+=============================================================================
//
// file :         DDThread.cpp
//
// description :  Include for the DDThread class.
//                This class is used for reading DD data (TBT)
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaSparkSRBPM_ns
{
class DDThread;
}

#include <DDThread.h>

namespace LiberaSparkSRBPM_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
DDThread::DDThread(LiberaSparkSRBPM *libera, PosCalculation *pc,omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), PC(pc), ds(libera)
{
  INFO_STREAM << "DDThread: Starting DD Thread." << endl;

  // Initialise connection parameters
  if( ds->attr_TBT_Dec_Enable_read[0])
    // Decimated data
    m_dod = std::dynamic_pointer_cast<RSource>(ds->tbtdecSignal);
  else
    // Normal data
    m_dod = std::dynamic_pointer_cast<RSource>(ds->tbtSignal);
  m_dodClient = NULL;

  threadStatus = "DDThread: acquiring";
  start_undetached();
}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *DDThread::run_undetached(void *arg) {

  string errorStr;

  exitThread = false;
  isConnected = false;

  cout << "DDThread: entering." << endl;

  while(!exitThread) {

    isConnected = OpenDod();
    if( isConnected ) {

      // Read data
      isig::SignalMeta signal_meta;
      size_t offset_read = (size_t)ds->attr_TBT_Offset_read[0];
      if (m_buf->GetLength() != (size_t)ds->attr_TBT_BufSize_read[0])
        m_buf->Resize((size_t)ds->attr_TBT_BufSize_read[0]);

      isig::SuccessCode_e res = isig::eSuccess;

      try {


        res = m_dodClient->Read(*m_buf, signal_meta, offset_read);

        if (res != isig::eSuccess) {

          if( !exitThread ) {
            cerr << "DDThread: Failed to read buffer, Error:" << res << endl;
            mutex.lock();
            threadStatus = "Failed to read buffer, Error code:" + std::to_string(res);
            mutex.unlock();
            m_dodClient->Close();
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
          }

        } else {

          computePosition(m_buf, m_buf->GetLength());

        }

      } catch (istd::Exception &e) {

        if( !exitThread ) {
          mutex.lock();
          threadStatus = "Failed to read buffer, Error :" + string(e.what());
          mutex.unlock();
          cerr << "DDThread: Failed to read buffer, Error:" << e.what() << endl;
          m_dodClient->Close();
          std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }

      }

    } else {

      // Connection failure
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    }

  }

  cout << "DDThread: disconnecting." << endl;

  if (m_dodClient && m_dodClient->IsOpen())
    m_dodClient->Close();

  cout << "DDThread: exiting." << endl;

  return NULL;

}

// ----------------------------------------------------------------------------------------

bool DDThread::OpenDod() {

  if (m_dodClient && m_dodClient->IsOpen()) {
    // Already connected
    return true;
  }

  size_t length = (size_t)ds->attr_TBT_BufSize_read[0];
  size_t offset = (size_t)ds->attr_TBT_Offset_read[0];

  m_dodClient = std::make_shared<DodClient>(m_dod, "dod_client", m_dod->GetTraits());
  m_buf =  std::make_shared<Int32Buffer>(m_dodClient->CreateBuffer(length));
  isig::SuccessCode_e res = m_dodClient->Open(isig::eModeDodOnEvent,length,offset);
  if (res != isig::eSuccess) {
    mutex.lock();
    threadStatus = "Failed to open DoD, Error :" + std::to_string(res);
    mutex.unlock();
    cerr << "DDThread::Failed to open DoD " << endl;
    return false;
  }

  cout << "DDThread: " << m_dod->GetName() << " DoD opened succesfully." << endl;

  return true;

}


// ----------------------------------------------------------------------------------------

#define GET_VALUES(field)                      \
  double *ret = NULL;                          \
  omni_mutex_lock l(mutex);                    \
  *length = ddValues.size();                   \
  if(ddValues.size()>0) {                      \
    ret = new double[ddValues.size()];         \
    for(int i=0;i<(int)ddValues.size();i++)    \
      ret[i] = ddValues[i].field;              \
  }                                            \
  return ret;

double *DDThread::getVa(int *length) {
  GET_VALUES(va);
}
double *DDThread::getVb(int *length) {
  GET_VALUES(vb);
}
double *DDThread::getVc(int *length) {
  GET_VALUES(vc);
}
double *DDThread::getVd(int *length) {
  GET_VALUES(vd);
}
double *DDThread::getSum(int *length) {
  GET_VALUES(sum);
}
double *DDThread::getQ(int *length) {
  GET_VALUES(q);
}
double *DDThread::getH(int *length) {
  GET_VALUES(h);
}
double *DDThread::getV(int *length) {
  GET_VALUES(v);
}
bool DDThread::hasData() {
  omni_mutex_lock l(mutex);
  return ddValues.size()>0;
}

// ----------------------------------------------------------------------------------------


void DDThread::computePosition(std::shared_ptr<Int32Buffer> dd,size_t nb) {

  mutex.lock();

  int off = 0;
  if( ds->attr_TBT_AntiSmearing_read[0] ) {
    nb -= 8;
    off = 1;
  }

  ddValues.clear();
  ddValues.reserve(nb);
  for (int i = 0; i < (int) nb; i++) {

    BP p;
    p.va = (*dd)[i+off][0];
    p.vb = (*dd)[i+off][1];
    p.vc = (*dd)[i+off][2];
    p.vd = (*dd)[i+off][3];
    PC->ComputeDDPosition(p);
    ddValues.push_back(p);

  }

  mutex.unlock();

  ds->push_data_ready_event("TBT_DataReady");

}

// ----------------------------------------------------------------------------------------

void DDThread::wakeup() {
  // Wake up thread
  cout << "DDThread: wake up." << endl;
  if (m_dodClient && m_dodClient->IsOpen()) {
    m_dodClient->Close();
  }

}

} // namespace LiberaSparkSRBPM_ns
