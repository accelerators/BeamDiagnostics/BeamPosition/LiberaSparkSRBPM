//+=============================================================================
//
// file :         ADCThread.cpp
//
// description :  Include for the ADCThread class.
//                This class is used for reading ADC data
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaSparkSRBPM_ns
{
class ADCThread;
}

#include <ADCThread.h>

namespace LiberaSparkSRBPM_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
ADCThread::ADCThread(LiberaSparkSRBPM *libera, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), ds(libera)
{
  INFO_STREAM << "ADCThread: Starting ADC Thread." << endl;

  // Initialise connection parameters
  m_dod = std::dynamic_pointer_cast<RADC>(ds->adcSignal);
  m_dodClient = NULL;
  threadStatus = "ADCThread: acquiring";
  start_undetached();
}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *ADCThread::run_undetached(void *arg) {


  exitThread = false;
  isConnected = false;

  cout << "ADCThread: entering." << endl;

  while(!exitThread) {

    isConnected = OpenDod();
    if( isConnected ) {

      // Read data
      isig::SignalMeta signal_meta;
      size_t offset_read = 0;
      if (m_buf->GetLength() != (size_t)ds->attr_ADC_BufSize_read[0])
        m_buf->Resize((size_t)ds->attr_ADC_BufSize_read[0]);

      isig::SuccessCode_e res = isig::eSuccess;

      try {

        res = m_dodClient->Read(*m_buf, signal_meta, offset_read);

        if (res != isig::eSuccess) {

          if( !exitThread ) {
            cerr << "ADCThread: Failed to read buffer, Error:" << res << endl;
            mutex.lock();
            threadStatus = "Failed to read buffer, Error code:" + std::to_string(res);
            mutex.unlock();
            m_dodClient->Close();
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
          }

        } else {

          mutex.lock();
          adcValues.clear();
          for (int i = 0; i < (int)m_buf->GetLength(); i++) {
            ADC p;
            p.va = (*m_buf)[i][0];
            p.vb = (*m_buf)[i][1];
            p.vc = (*m_buf)[i][2];
            p.vd = (*m_buf)[i][3];
            adcValues.push_back(p);
          }
          mutex.unlock();
          ds->push_data_ready_event("TBT_DataReady");

        }

      } catch (istd::Exception &e) {
        if( !exitThread ) {
          mutex.lock();
          threadStatus = "Failed to read buffer, Error :" + string(e.what());
          mutex.unlock();
          cerr << "ADCThread: Failed to read buffer, Error:" << e.what() << endl;
          m_dodClient->Close();
          std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
      }

    } else {

      // Connection failure
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    }

  }

  cout << "ADCThread: disconnecting." << endl;

  if (m_dodClient && m_dodClient->IsOpen())
    m_dodClient->Close();

  cout << "ADCThread: exiting." << endl;
  return NULL;

}

// ----------------------------------------------------------------------------------------

bool ADCThread::OpenDod() {

  if (m_dodClient && m_dodClient->IsOpen()) {
    // Already connected
    return true;
  }

  size_t length = (size_t)ds->attr_ADC_BufSize_read[0];

  m_dodClient = std::make_shared<ADCClient>(m_dod, "dod_client", m_dod->GetTraits());
  m_buf =  std::make_shared<Int16Buffer>(m_dodClient->CreateBuffer(length));
  isig::SuccessCode_e res = m_dodClient->Open(isig::eModeDodOnEvent,length,0);
  if (res != isig::eSuccess) {
    mutex.lock();
    threadStatus = "Failed to open DoD, Error :" + std::to_string(res);
    mutex.unlock();
    cerr << "ADCThread::Failed to open DoD " << endl;
    return false;
  }

  cout << "ADCThread: " << m_dod->GetName() << " DoD opened succesfully." << endl;

  return true;

}

// ----------------------------------------------------------------------------------------

#define GET_VALUES(field)                      \
  int16_t *ret = NULL;                         \
  omni_mutex_lock l(mutex);                    \
  *length = adcValues.size();                  \
  if(adcValues.size()>0) {                     \
    ret = new int16_t[adcValues.size()];       \
    for(int i=0;i<(int)adcValues.size();i++)   \
      ret[i] = adcValues[i].field;             \
  }                                            \
  return ret;

int16_t *ADCThread::getVa(int *length) {
  GET_VALUES(va);
}
int16_t *ADCThread::getVb(int *length) {
  GET_VALUES(vb);
}
int16_t *ADCThread::getVc(int *length) {
  GET_VALUES(vc);
}
int16_t *ADCThread::getVd(int *length) {
  GET_VALUES(vd);
}
bool ADCThread::hasData() {
  omni_mutex_lock l(mutex);
  return adcValues.size()>0;
}


// ----------------------------------------------------------------------------------------

void ADCThread::wakeup() {
  // Wake up thread
  cout << "ADCThread: wake up." << endl;
  if (m_dodClient && m_dodClient->IsOpen()) {
    m_dodClient->Close();
  }
}

} // namespace LiberaSparkSRBPM_ns
