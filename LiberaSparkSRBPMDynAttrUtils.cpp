/*----- PROTECTED REGION ID(LiberaSparkSRBPM::DynAttrUtils.cpp) ENABLED START -----*/
/* clang-format on */
//=============================================================================
//
// file :        LiberaSparkSRBPMDynAttrUtils.cpp
//
// description : Dynamic attributes utilities file for the LiberaSparkSRBPM class
//
// project :     LiberaSparkBPM
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//
// Copyright (C): 2019
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <LiberaSparkSRBPM.h>
#include <LiberaSparkSRBPMClass.h>
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::DynAttrUtils.cpp

//================================================================
//  Attributes managed are:
//================================================================
//  SA_Angle       |  Tango::DevDouble	Scalar
//  Phase          |  Tango::DevDouble	Scalar
//  Phase_History  |  Tango::DevDouble	Spectrum  ( max = 2000)
//================================================================

//	For compatibility reason, this file (LiberaSparkSRBPMDynAttrUtils)
//	manage also the dynamic command utilities.
//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//================================================================

namespace LiberaSparkSRBPM_ns
{
//=============================================================
//	Add/Remove dynamic attribute methods
//=============================================================

//--------------------------------------------------------
/**
 *	Add a SA_Angle dynamic attribute.
 *
 *  parameter attname: attribute name to be created and added.
 */
//--------------------------------------------------------
void LiberaSparkSRBPM::add_SA_Angle_dynamic_attribute(std::string attname)
{
	//	Attribute : SA_Angle
	SA_AngleAttrib	*sa_angle = new SA_AngleAttrib(attname);
	Tango::UserDefaultAttrProp	sa_angle_prop;
	//	description	not set for SA_Angle
	//	label	not set for SA_Angle
	sa_angle_prop.set_unit("rad");
	//	standard_unit	not set for SA_Angle
	//	display_unit	not set for SA_Angle
	sa_angle_prop.set_format("%.2f");
	//	max_value	not set for SA_Angle
	//	min_value	not set for SA_Angle
	//	max_alarm	not set for SA_Angle
	//	min_alarm	not set for SA_Angle
	//	max_warning	not set for SA_Angle
	//	min_warning	not set for SA_Angle
	//	delta_t	not set for SA_Angle
	//	delta_val	not set for SA_Angle
	/*----- PROTECTED REGION ID(LiberaSparkSRBPM::att_SA_Angle_dynamic_attribute) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::att_SA_Angle_dynamic_attribute
	sa_angle->set_default_properties(sa_angle_prop);
	//	Not Polled
	sa_angle->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	SA_Angle_data.insert(make_pair(attname, 0.0));
	add_attribute(sa_angle);
}
//--------------------------------------------------------
/**
 *	remove a SA_Angle dynamic attribute.
 *
 *  parameter attname: attribute name to be removed.
 */
//--------------------------------------------------------
void LiberaSparkSRBPM::remove_SA_Angle_dynamic_attribute(std::string attname)
{
	remove_attribute(attname, true, Tango::Util::instance()->_UseDb);
	std::map<std::string,Tango::DevDouble>::iterator ite;
	if ((ite=SA_Angle_data.find(attname))!=SA_Angle_data.end())
	{
		/*----- PROTECTED REGION ID(LiberaSparkSRBPM::remove_SA_Angle_dynamic_attribute) ENABLED START -----*/
		/* clang-format on */
		//	Add your own code
		/* clang-format off */
		/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::remove_SA_Angle_dynamic_attribute
		SA_Angle_data.erase(ite);
	}
}
//--------------------------------------------------------
/**
 *	Add a Phase dynamic attribute.
 *
 *  parameter attname: attribute name to be created and added.
 */
//--------------------------------------------------------
void LiberaSparkSRBPM::add_Phase_dynamic_attribute(std::string attname)
{
	//	Attribute : Phase
	PhaseAttrib	*phase = new PhaseAttrib(attname);
	Tango::UserDefaultAttrProp	phase_prop;
	//	description	not set for Phase
	//	label	not set for Phase
	phase_prop.set_unit("deg");
	//	standard_unit	not set for Phase
	//	display_unit	not set for Phase
	//	format	not set for Phase
	//	max_value	not set for Phase
	//	min_value	not set for Phase
	//	max_alarm	not set for Phase
	//	min_alarm	not set for Phase
	//	max_warning	not set for Phase
	//	min_warning	not set for Phase
	//	delta_t	not set for Phase
	//	delta_val	not set for Phase
	/*----- PROTECTED REGION ID(LiberaSparkSRBPM::att_Phase_dynamic_attribute) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::att_Phase_dynamic_attribute
	phase->set_default_properties(phase_prop);
	//	Not Polled
	phase->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	Phase_data.insert(make_pair(attname, 0.0));
	add_attribute(phase);
}
//--------------------------------------------------------
/**
 *	remove a Phase dynamic attribute.
 *
 *  parameter attname: attribute name to be removed.
 */
//--------------------------------------------------------
void LiberaSparkSRBPM::remove_Phase_dynamic_attribute(std::string attname)
{
	remove_attribute(attname, true, Tango::Util::instance()->_UseDb);
	std::map<std::string,Tango::DevDouble>::iterator ite;
	if ((ite=Phase_data.find(attname))!=Phase_data.end())
	{
		/*----- PROTECTED REGION ID(LiberaSparkSRBPM::remove_Phase_dynamic_attribute) ENABLED START -----*/
		/* clang-format on */
		//	Add your own code
		/* clang-format off */
		/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::remove_Phase_dynamic_attribute
		Phase_data.erase(ite);
	}
}
//--------------------------------------------------------
/**
 *	Add a Phase_History dynamic attribute.
 *
 *  parameter attname: attribute name to be created and added.
 *  parameter ptr:     memory buffer used to set attribute value.
 *                     If NULL or not specified, buffer will be allocated.
 */
//--------------------------------------------------------
void LiberaSparkSRBPM::add_Phase_History_dynamic_attribute(std::string attname, Tango::DevDouble *ptr)
{
	//	Attribute : Phase_History
	if (ptr==NULL)
		ptr = new Tango::DevDouble[2000];
	Phase_HistoryAttrib	*phase_history = new Phase_HistoryAttrib(attname);
	Tango::UserDefaultAttrProp	phase_history_prop;
	//	description	not set for Phase_History
	//	label	not set for Phase_History
	phase_history_prop.set_unit("deg");
	//	standard_unit	not set for Phase_History
	//	display_unit	not set for Phase_History
	//	format	not set for Phase_History
	//	max_value	not set for Phase_History
	//	min_value	not set for Phase_History
	//	max_alarm	not set for Phase_History
	//	min_alarm	not set for Phase_History
	//	max_warning	not set for Phase_History
	//	min_warning	not set for Phase_History
	//	delta_t	not set for Phase_History
	//	delta_val	not set for Phase_History
	/*----- PROTECTED REGION ID(LiberaSparkSRBPM::att_Phase_History_dynamic_attribute) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::att_Phase_History_dynamic_attribute
	phase_history->set_default_properties(phase_history_prop);
	//	Not Polled
	phase_history->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	Phase_History_data.insert(make_pair(attname, ptr));
	add_attribute(phase_history);
}
//--------------------------------------------------------
/**
 *	remove a Phase_History dynamic attribute.
 *
 *  parameter attname: attribute name to be removed.
 *  parameter free_it: memory buffer will be freed if true or not specified.
 */
//--------------------------------------------------------
void LiberaSparkSRBPM::remove_Phase_History_dynamic_attribute(std::string attname, bool free_it)
{
	remove_attribute(attname, true, Tango::Util::instance()->_UseDb);
	std::map<std::string,Tango::DevDouble *>::iterator ite;
	if ((ite=Phase_History_data.find(attname))!=Phase_History_data.end())
	{
		/*----- PROTECTED REGION ID(LiberaSparkSRBPM::remove_Phase_History_dynamic_attribute) ENABLED START -----*/
		/* clang-format on */
		//	Add your own code
		/* clang-format off */
		/*----- PROTECTED REGION END -----*/	//	LiberaSparkSRBPM::remove_Phase_History_dynamic_attribute
		if (free_it)
			delete[] ite->second;
		Phase_History_data.erase(ite);
	}
}


//============================================================
//	Tool methods to get pointer on attribute data buffer
//============================================================
//--------------------------------------------------------
/**
 *	Return a pointer on SA_Angle data.
 *
 *  parameter attname: the specified attribute name.
 */
//--------------------------------------------------------
Tango::DevDouble *LiberaSparkSRBPM::get_SA_Angle_data_ptr(std::string &name)
{
	std::map<std::string,Tango::DevDouble>::iterator ite;
	if ((ite=SA_Angle_data.find(name))==SA_Angle_data.end())
	{
		TangoSys_OMemStream	tms;
		tms << "Dynamic attribute " << name << " has not been created";
		Tango::Except::throw_exception(
					(const char *)"ATTRIBUTE_NOT_FOUND",
					tms.str().c_str(),
					(const char *)"LiberaSparkSRBPM::get_SA_Angle_data_ptr()");
	}
	return  &(ite->second);
}
//--------------------------------------------------------
/**
 *	Return a pointer on Phase data.
 *
 *  parameter attname: the specified attribute name.
 */
//--------------------------------------------------------
Tango::DevDouble *LiberaSparkSRBPM::get_Phase_data_ptr(std::string &name)
{
	std::map<std::string,Tango::DevDouble>::iterator ite;
	if ((ite=Phase_data.find(name))==Phase_data.end())
	{
		TangoSys_OMemStream	tms;
		tms << "Dynamic attribute " << name << " has not been created";
		Tango::Except::throw_exception(
					(const char *)"ATTRIBUTE_NOT_FOUND",
					tms.str().c_str(),
					(const char *)"LiberaSparkSRBPM::get_Phase_data_ptr()");
	}
	return  &(ite->second);
}
//--------------------------------------------------------
/**
 *	Return a pointer on Phase_History data.
 *
 *  parameter attname: the specified attribute name.
 */
//--------------------------------------------------------
Tango::DevDouble *LiberaSparkSRBPM::get_Phase_History_data_ptr(std::string &name)
{
	std::map<std::string,Tango::DevDouble *>::iterator ite;
	if ((ite=Phase_History_data.find(name))==Phase_History_data.end())
	{
		TangoSys_OMemStream	tms;
		tms << "Dynamic attribute " << name << " has not been created";
		Tango::Except::throw_exception(
					(const char *)"ATTRIBUTE_NOT_FOUND",
					tms.str().c_str(),
					(const char *)"LiberaSparkSRBPM::get_Phase_History_data_ptr()");
	}
	return  ite->second;
}


//=============================================================
//	Add/Remove dynamic command methods
//=============================================================


} //	namespace
