//+=============================================================================
//
// file :         DDThread.h
//
// description :  Include for the DDThread class.
//                This class is used for reading DD data (TBT)
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERASPARKSRBPM_DDTHREAD_H
#define LIBERASPARKSRBPM_DDTHREAD_H

#include <LiberaSparkSRBPM.h>

namespace LiberaSparkSRBPM_ns {

class DDThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    DDThread(LiberaSparkSRBPM *,PosCalculation *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

private:

    vector<BP> ddValues;
    LiberaSparkSRBPM *ds;
    omni_mutex &mutex;
    PosCalculation *PC;

    std::shared_ptr<RSource>      m_dod;
    std::shared_ptr<DodClient>    m_dodClient;
    std::shared_ptr<Int32Buffer>  m_buf;

    bool OpenDod();
    void computePosition(std::shared_ptr<Int32Buffer> dd,size_t nb);

public:

    double *getVa(int *length);
    double *getVb(int *length);
    double *getVc(int *length);
    double *getVd(int *length);
    double *getSum(int *length);
    double *getQ(int *length);
    double *getH(int *length);
    double *getV(int *length);
    bool hasData();

    void wakeup();

}; // class DDThread

} // namespace LiberaSparkSRBPM_ns

#endif //LIBERASPARKSRBPM_DDTHREAD_H
