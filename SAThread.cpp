//+=============================================================================
//
// file :         SAThread.cpp
//
// description :  This class is used for reading SAStream
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaSparkSRBPM_ns
{
class SAThread;
}

#include <SAThread.h>
#include "PosCalculation.h"
#include "LiberaSparkSRBPM.h"
#include <thread>

#define SQUARE(x) ((x)*(x))

namespace LiberaSparkSRBPM_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
SAThread::SAThread(LiberaSparkSRBPM *libera, PosCalculation *pc, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), PC(pc), ds(libera)
{
  INFO_STREAM << "SAThread: Starting SA Thread." << endl;

  m_stream = std::dynamic_pointer_cast<RStream>(ds->saSignal);

  historyLength = 1800;
  average = false;
  avgLength  = 20;
  statLength = 0;
  currentPos.va = NAN;
  currentPos.vb = NAN;
  currentPos.vc = NAN;
  currentPos.vd = NAN;
  currentPos.sum = NAN;
  currentPos.h = NAN;
  currentPos.v = NAN;
  currentPos.q = NAN;
  currentPos.aangle = NAN;
  currentPos.bangle = NAN;
  currentPos.cangle = NAN;
  currentPos.dangle = NAN;
  currentPos.vphase = NAN;
  currentPos.srphase = NAN;
  currentPos.syphase = NAN;
  HStdDev = NAN;
  VStdDev = NAN;
  threadStatus.clear();
  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *SAThread::run_undetached(void *arg) {

  string errorStr;

  exitThread = false;
  isConnected = false;

  cout << "SAThread: entering." << endl;

  while(!exitThread) {

    isConnected = OpenStream();
    if( isConnected ) {

      // Read data
      try {

        isig::SuccessCode_e res = m_streamClient->Read(*m_buf);
        if (res != isig::eSuccess) {
          mutex.lock();
          threadStatus = "Failed to read stream, Error code:" + std::to_string(res);
          mutex.unlock();
          m_streamClient->Close();
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        } else {

          // Two more samples
          // Reduce from 40Hz to 20Hz

          BP p;
          PC->ComputePosition(*m_buf, p);

          // Update buffers and values
          mutex.lock();

          saValues.push_back(p);
          while ((int) saValues.size() > historyLength)
            saValues.erase(saValues.begin());

          currentPos = p;

          computeAveraging();
          computeStats();

          threadStatus.clear();

          mutex.unlock();


        }

      } catch (istd::Exception &e) {
        mutex.lock();
        threadStatus = "Failed to read stream, Error :" + string(e.what());
        mutex.unlock();
        cerr << "SAThread: Failed to read stream, Error:" << e.what() << endl;
        m_streamClient->Close();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
      }

    } else {

      // Connection failure
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    }

  }

  cout << "SAThread: disconnecting." << endl;

  if (m_streamClient && m_streamClient->IsOpen())
    m_streamClient->Close();

  cout << "SAThread: exiting." << endl;

  return NULL;

}

// ----------------------------------------------------------------------------------------

bool SAThread::OpenStream() {

  if (m_streamClient && m_streamClient->IsOpen()) {
    // Already connected
    return true;
  }

  m_streamClient = std::make_shared<StreamClient>(m_stream.get(), "stream_client");
  m_buf =  std::make_shared<Int32Buffer>(m_streamClient->CreateBuffer(2));
  isig::SuccessCode_e res = m_streamClient->Open();
  if (res != isig::eSuccess) {
    mutex.lock();
    threadStatus = "Failed to read stream, Error :" + std::to_string(res);
    mutex.unlock();
    cerr << "SAThread::Failed to open SA stream" << endl;
    return false;
  }
  m_streamClient->SetReadTimeout(std::chrono::milliseconds(100));

  return true;

}

// ----------------------------------------------------------------------------------------

void SAThread::computeAveraging() {

  if(average) {

    int hSize = (int)saValues.size();
    int start = hSize - avgLength;
    int length = avgLength;
    if(start<0)
      length += start;

    double sumx = 0;
    double sumz = 0;
    double sums = 0;
    for(int i=0;i<length;i++) {
      sumx += saValues[hSize-(i+1)].h;
      sumz += saValues[hSize-(i+1)].v;
      sums += saValues[hSize-(i+1)].sum;
    }
    currentPos.h = sumx / (double)length;
    currentPos.v = sumz / (double)length;
    currentPos.sum = sums / (double)length;

  }

}

void SAThread::computeStats() {

  if(statLength>0) {

    int hSize = (int)saValues.size();
    int start = hSize - statLength;
    int length = statLength;
    if(start<0)
      length += start;

    double sumx = 0;
    double sumz = 0;
    double sumx2 = 0;
    double sumz2 = 0;
    for(int i=0;i<length;i++) {
      sumx += saValues[hSize-(i+1)].h;
      sumz += saValues[hSize-(i+1)].v;
      sumx2 += SQUARE(saValues[hSize-(i+1)].h);
      sumz2 += SQUARE(saValues[hSize-(i+1)].v);
    }

    double avgx = sumx / (double)length;
    double avgz = sumz / (double)length;
    double avgx2 = sumx2 / (double)length;
    double avgz2 = sumz2 / (double)length;

    HStdDev = sqrt( avgx2 - SQUARE(avgx) );
    VStdDev = sqrt( avgz2 - SQUARE(avgz) );

  } else {

    HStdDev = NAN;
    VStdDev = NAN;

  }

}

// ----------------------------------------------------------------------------------------

void SAThread::wakeup() {

}

// ----------------------------------------------------------------------------------------
// Scalar values
// ----------------------------------------------------------------------------------------
#define GET_VAL(v)        \
omni_mutex_lock l(mutex); \
if(!isConnected)          \
  return NAN;             \
else                      \
  return v;

double SAThread::GetX() {

  GET_VAL(currentPos.h);

}

double SAThread::GetZ() {

  GET_VAL(currentPos.v);

}

double SAThread::GetQ() {

  GET_VAL(currentPos.q);

}

double SAThread::GetVa() {

  GET_VAL(currentPos.va);

}

double SAThread::GetVb() {

  GET_VAL(currentPos.vb);

}

double SAThread::GetVc() {

  GET_VAL(currentPos.vc);

}

double SAThread::GetVd() {

  GET_VAL(currentPos.vd);

}

double SAThread::GetSum() {

  GET_VAL(currentPos.sum);

}

double SAThread::GetXStdDev() {

  GET_VAL(HStdDev);

}

double SAThread::GetZStdDev() {

  GET_VAL(VStdDev);

}

double SAThread::GetAAngle() {

  GET_VAL(currentPos.aangle);

}
double SAThread::GetBAngle() {

  GET_VAL(currentPos.bangle);

}
double SAThread::GetCAngle() {

  GET_VAL(currentPos.cangle);

}
double SAThread::GetDAngle() {

  GET_VAL(currentPos.dangle);

}
double SAThread::GetVerifPhase() {

  GET_VAL(currentPos.vphase);

}
double SAThread::GetSRPhase() {

  GET_VAL(currentPos.srphase);

}
double SAThread::GetSYPhase() {

  GET_VAL(currentPos.syphase);

}

// ----------------------------------------------------------------------------------------
// Config values
// ----------------------------------------------------------------------------------------

void SAThread::setHistoryLength(int length) {

  omni_mutex_lock l(mutex);
  historyLength = length;

}

int  SAThread::getHistoryLength() {

  return historyLength;

}

void SAThread::setStatLength(int length) {
  omni_mutex_lock l(mutex);
  statLength = length;
}

int SAThread::getStatLength() {
  return statLength;
}

void SAThread::setAveraging(bool avg) {

  average = avg;

}

bool SAThread::isAveraging() {

  return average;

}

void SAThread::setAvgLength(int length) {

  omni_mutex_lock l(mutex);
  avgLength = length;

}

int SAThread::getAvgLength() {

  return avgLength;

}

// ----------------------------------------------------------------------------------------
// History values
// ----------------------------------------------------------------------------------------

#define GET_HIST(field)                   \
  for(int i=0;i<(int)saValues.size();i++) \
    hist[i]=saValues[i].field;            \
  *length = saValues.size();

void SAThread::getVaHistory(double *hist,int *length) {
  GET_HIST(va);
}

void SAThread::getVbHistory(double *hist,int *length) {
  GET_HIST(vb);
}

void SAThread::getVcHistory(double *hist,int *length) {
  GET_HIST(vc);
}

void SAThread::getVdHistory(double *hist,int *length) {
  GET_HIST(vd);
}

void SAThread::getSumHistory(double *hist,int *length) {
  GET_HIST(sum);
}

void SAThread::getXHistory(double *hist,int *length) {
  GET_HIST(h);
}

void SAThread::getZHistory(double *hist,int *length) {
  GET_HIST(v);
}

void SAThread::getQHistory(double *hist,int *length) {
  GET_HIST(q);
}

void SAThread::getVerifPhaseHistory(double *hist,int *length) {
  GET_HIST(vphase);
}

void SAThread::getSRPhaseHistory(double *hist,int *length) {
  GET_HIST(srphase);
}

void SAThread::getSYPhaseHistory(double *hist,int *length) {
  GET_HIST(syphase);
}

} // namespace LiberaBrilliance_ns
