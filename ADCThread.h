//+=============================================================================
//
// file :         ADCThread.h
//
// description :  Include for the ADCThread class.
//                This class is used for reading ADC data
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERASPARKSRBPM_ADCTHREAD_H
#define LIBERASPARKSRBPM_ADCTHREAD_H

#include <LiberaSparkSRBPM.h>

namespace LiberaSparkSRBPM_ns {

class ADCThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    ADCThread(LiberaSparkSRBPM *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

 private:

    vector<ADC> adcValues;
    LiberaSparkSRBPM *ds;
    omni_mutex &mutex;

    std::shared_ptr<RADC>         m_dod;
    std::shared_ptr<ADCClient>    m_dodClient;
    std::shared_ptr<Int16Buffer>  m_buf;

    bool OpenDod();

public:

    int16_t *getVa(int *length);
    int16_t *getVb(int *length);
    int16_t *getVc(int *length);
    int16_t *getVd(int *length);
    bool hasData();
    void wakeup();

}; // class ADCThread

} // namespace LiberaSparkSRBPM_ns

#endif //LIBERASPARKSRBPM_ADCTHREAD_H
