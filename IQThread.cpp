//+=============================================================================
//
// file :         IQThread.cpp
//
// description :  Include for the IQThread class.
//                This class is used for reading DD IQ data (TBT)
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaSparkSRBPM_ns
{
class IQThread;
}

#include <IQThread.h>

namespace LiberaSparkSRBPM_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
IQThread::IQThread(LiberaSparkSRBPM *libera, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), ds(libera)
{
  INFO_STREAM << "IQThread: Starting IQ Thread." << endl;

  // Initialise connection parameters
  if( ds->attr_TBT_Dec_Enable_read[0])
    // Decimated data
    m_dod = std::dynamic_pointer_cast<RSource>(ds->tbtiqdecSignal);
  else
    // Normal data
    m_dod = std::dynamic_pointer_cast<RSource>(ds->tbtiqSignal);
  m_dodClient = NULL;

  threadStatus = "IQThread: acquiring";
  start_undetached();
}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *IQThread::run_undetached(void *arg) {

  string errorStr;

  exitThread = false;
  isConnected = false;

  cout << "IQThread: entering." << endl;

  while(!exitThread) {

    isConnected = OpenDod();
    if( isConnected ) {

      // Read data
      isig::SignalMeta signal_meta;
      size_t offset_read = (size_t)ds->attr_TBT_Offset_read[0];
      if (m_buf->GetLength() != (size_t)ds->attr_TBT_BufSize_read[0])
        m_buf->Resize((size_t)ds->attr_TBT_BufSize_read[0]);

      isig::SuccessCode_e res = isig::eSuccess;

      try {

        res = m_dodClient->Read(*m_buf, signal_meta, offset_read);

        if (res != isig::eSuccess) {

          if( !exitThread ) {
            cerr << "IQThread: Failed to read buffer, Error:" << res << endl;
            mutex.lock();
            threadStatus = "Failed to read buffer, Error code:" + std::to_string(res);
            mutex.unlock();
            m_dodClient->Close();
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
          }

        } else {

          computePosition(m_buf, m_buf->GetLength());

        }

      } catch (istd::Exception &e) {
        if( !exitThread ) {
          mutex.lock();
          threadStatus = "Failed to read buffer, Error :" + string(e.what());
          mutex.unlock();
          cerr << "IQThread: Failed to read buffer, Error:" << e.what() << endl;
          m_dodClient->Close();
          std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
      }

    } else {

      // Connection failure
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    }

  }

  cout << "IQThread: disconnecting." << endl;

  if (m_dodClient && m_dodClient->IsOpen())
    m_dodClient->Close();

  cout << "IQThread: exiting." << endl;

  return NULL;

}

// ----------------------------------------------------------------------------------------

bool IQThread::OpenDod() {

  if (m_dodClient && m_dodClient->IsOpen()) {
    // Already connected
    return true;
  }

  size_t length = (size_t)ds->attr_TBT_BufSize_read[0];
  size_t offset = (size_t)ds->attr_TBT_Offset_read[0];

  m_dodClient = std::make_shared<DodClient>(m_dod, "dod_client", m_dod->GetTraits());
  m_buf =  std::make_shared<Int32Buffer>(m_dodClient->CreateBuffer(length));
  isig::SuccessCode_e res = m_dodClient->Open(isig::eModeDodOnEvent,length,offset);
  if (res != isig::eSuccess) {
    mutex.lock();
    threadStatus = "Failed to open DoD, Error :" + std::to_string(res);
    mutex.unlock();
    cerr << "IQThread::Failed to open DoD " << endl;
    return false;
  }

  cout << "IQThread: " << m_dod->GetName() << " DoD opened succesfully." << endl;

  return true;

}


// ----------------------------------------------------------------------------------------

#define GET_VALUES(field)                      \
  double *ret = NULL;                          \
  omni_mutex_lock l(mutex);                    \
  *length = iqValues.size();                   \
  if(iqValues.size()>0) {                      \
    ret = new double[iqValues.size()];         \
    for(int i=0;i<(int)iqValues.size();i++)    \
      ret[i] = iqValues[i].field;              \
  }                                            \
  return ret;


double *IQThread::getIa(int *length) {
  GET_VALUES(ia);
}
double *IQThread::getIb(int *length) {
  GET_VALUES(ib);
}
double *IQThread::getIc(int *length) {
  GET_VALUES(ic);
}
double *IQThread::getId(int *length) {
  GET_VALUES(id);
}
double *IQThread::getQa(int *length) {
  GET_VALUES(qa);
}
double *IQThread::getQb(int *length) {
  GET_VALUES(qb);
}
double *IQThread::getQc(int *length) {
  GET_VALUES(qc);
}
double *IQThread::getQd(int *length) {
  GET_VALUES(qd);
}
bool IQThread::hasData() {
  omni_mutex_lock l(mutex);
  return iqValues.size()>0;
}


// ----------------------------------------------------------------------------------------


void IQThread::computePosition(std::shared_ptr<Int32Buffer> dd,size_t nb) {

  mutex.lock();

  iqValues.clear();
  iqValues.reserve(nb);
  for (int i = 0; i < (int) nb; i++) {

    BP p;
    p.ia = (*dd)[i][0];
    p.qa = (*dd)[i][1];
    p.ib = (*dd)[i][2];
    p.qb = (*dd)[i][3];
    p.ic = (*dd)[i][4];
    p.qc = (*dd)[i][5];
    p.id = (*dd)[i][6];
    p.qd = (*dd)[i][7];
    iqValues.push_back(p);

  }

  mutex.unlock();

  ds->push_data_ready_event("TBT_IQ_DataReady");

}

// ----------------------------------------------------------------------------------------

void IQThread::wakeup() {
  // Wake up thread
  cout << "IQThread: wake up." << endl;
  if (m_dodClient && m_dodClient->IsOpen()) {
    m_dodClient->Close();
  }

}

} // namespace LiberaSparkSRBPM_ns
