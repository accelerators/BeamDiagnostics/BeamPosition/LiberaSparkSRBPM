//+=============================================================================
//
// file :         PositionCalculation.h
//
// description :  Include for the PositionCalculation class.
//                This class is used for computing beam position
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERASPARKSRBPM_POSCALCULATION_H
#define LIBERASPARKSRBPM_POSCALCULATION_H

#include "LiberaSparkSRBPM.h"

#define DOS_CALC 0
#define POLYNOMIAL_CALC 1

namespace LiberaSparkSRBPM_ns {


class PosCalculation {

public:

    PosCalculation(LiberaSparkSRBPM *ds);
    void ComputePosition(Int32Buffer &sa, BP &p);
    void ComputeDDPosition(BP &p);
    void SetAlgorithm(int algo);
    int  GetAlgorithm();
    void SetTiltAngle(double angle);
    void SetPolynomial(vector<POLY>& hPoly,vector<POLY>& vPoly,vector<POLY>& qPoly,vector<POLY>& sPoly);

    double kA;
    double kB;
    double kC;
    double kD;
    double tiltAngle;

private:
    LiberaSparkSRBPM *ds;

    void ComputePosition(BP &p);
    int calculationAlgorithm;

    // Polynomial stuff
    vector<POLY> hPoly;
    vector<POLY> vPoly;
    vector<POLY> qPoly;
    vector<POLY> sPoly;

    double px[MAX_POLYNOMIAL_POWER];
    double py[MAX_POLYNOMIAL_POWER];
    int maxXPow;
    int maxYPow;

    double r11,r12,r21,r22;

    double sumSAThreshold;

};

} // LiberaBrilliance_ns

#endif //LIBERASPARKSRBPM_POSCALCULATION_H
