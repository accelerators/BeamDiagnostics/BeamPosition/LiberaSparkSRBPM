# LiberaSparkSRBPM

A Tango server for the LiberaSpark SRBPM.

## Cloning 

```
git clone git@gitlab.esrf.fr:accelerators/RadioFrequency/ssa/SSAADC.git
```

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.
* MCI 3.0

#### Toolchain Dependencies

* C++11 compliant compiler for ARM (arm-xilinx-linux-gnueabi)

### Build

```
cd LiberaSparkSRBPM
make
```

## Deployement

Binary is deployed there:
```
/operation/control/infra/equipment/sparks/spark_srbpm/libera/sbin
```

### Auto Gain Control

2 device properties are availables:
```
AGCMinADC: (Default 3000) // if( MAxADC < AGCMinADC ) attenuation -= 3
AGCMaxADC: (Default 7000) // if( MAxADC > AGCMaxADC ) attenuation += 1
```

### Polynomial

Polynomial coeficients are defined by a set of 2 class properties of the class LiberaSparkSRBPM starting with H_BPM_Coef_ and V_BPM_Coef_ followed by an arbitraty suffix. The suffix defines the set name (see BPM_Set_List class property). The coeficient are given in the following order:

i<sub>0</sub> * X<sup>0</sup>Y<sup>0</sup> + i<sub>1</sub> * X<sup>0</sup>Y<sup>1</sup> + ... + i<sub>n</sub> * X<sup>0</sup>Y<sup>n</sup> + i<sub>n+1</sub> * X<sup>1</sup>Y<sup>0</sup> + i<sub>n+2</sub> * X<sup>1</sup>Y<sup>1</sup> + ... + i<sub>2n</sub> * X<sup>1</sup>Y<sup>n-1</sup> +  i<sub>2n+1</sub> * X<sup>2</sup>Y<sup>0</sup> + ...

For a n<sup>th</sup> order polynomial, the number of coeficient must be equal to:

n*(n+1)/2 + (n+1)

When CalcAlgorithm attribute is set to 1, all positions within a BPM node (see bpm-ireg) are recomputed using the following forumla:

  a = V<sub>a</sub>*kA<br/>
  b = V<sub>b</sub>*kB<br/>
  c = V<sub>c</sub>*kC<br/>
  d = V<sub>d</sub>*kD<br/>
  X = (a-b-c+d)/(a+b+c+d)<br/>
  Y = (a+b-c-d)/(a+b+c+d)<br/>
  X<sub>pos</sub> = H_poly(X,Y) + H<sub>offset</sub><br/>
  Y<sub>pos</sub> = V_poly(X,Y) + V<sub>offset</sub><br/>

Where H<sub>offset</sub> and V<sub>offset</sub> are the same offsets as defined for the defaut DoS calculation.

The BPM_Set_List class property define the list of all the defined set. It appears in the Polynomial attribute and allow to switch between polynomial sets.

```  

# Example of definition of 2 polynomial set of order 1

CLASS/LiberaSparkSRBPM->BPM_Set_List: LargeROI,\ 
                                      SmallROI
CLASS/LiberaSparkSRBPM->H_BPM_Coef_LargeROI: 0,\ 
                                             10000000,\ 
                                             0
CLASS/LiberaSparkSRBPM->H_BPM_Coef_SmallROI: 0,\ 
                                             0,\ 
                                             10000000
CLASS/LiberaSparkSRBPM->V_BPM_Coef_LargeROI: 0,\ 
                                             0,\ 
                                             10000000
CLASS/LiberaSparkSRBPM->V_BPM_Coef_SmallROI: 0,\ 
                                             10000000,\ 
                                             0
```  

