//+=============================================================================
//
// file :         PositionCalculation.h
//
// description :  This class is used for computing beam position
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaSparkSRBPM_ns
{
class PosCalculation;
}

#include "PosCalculation.h"
#include "LiberaSparkSRBPM.h"

namespace LiberaSparkSRBPM_ns {


// --------------------------------------------------------------------------------------
#define SEARCH_MAX(poly)                        \
for(int i=0;i<(int)poly.size();i++) {           \
  if(poly[i].nX>maxXPow) maxXPow = poly[i].nX;  \
  if(poly[i].nY>maxYPow) maxYPow = poly[i].nY;  \
}

void PosCalculation::SetPolynomial(vector<POLY>& hPoly,vector<POLY>& vPoly,vector<POLY>& qPoly,vector<POLY>& sPoly) {

  this->hPoly = hPoly;
  this->vPoly = vPoly;
  this->qPoly = qPoly;
  this->sPoly = sPoly;
  maxXPow = 0;
  maxYPow = 0;
  SEARCH_MAX(hPoly);
  SEARCH_MAX(vPoly);
  SEARCH_MAX(qPoly);
  SEARCH_MAX(sPoly);

}

// --------------------------------------------------------------------------------------

void PosCalculation::SetAlgorithm(int algo) {

  calculationAlgorithm = algo;

}

// --------------------------------------------------------------------------------------

void PosCalculation::SetTiltAngle(double angle) {

  tiltAngle = angle;
  r11 = cos(angle);r12 = -sin(angle);
  r21 = sin(angle);r22 = cos(angle);

}

// --------------------------------------------------------------------------------------

int  PosCalculation::GetAlgorithm() {

  return calculationAlgorithm;

}

// --------------------------------------------------------------------------------------

PosCalculation::PosCalculation(LiberaSparkSRBPM *ds) {

  this->ds = ds;

  kA=1.0;
  kB=1.0;
  kC=1.0;
  kD=1.0;
  // Rotation matrix
  tiltAngle = 0.0;
  r11 = 1.0; r12 = 0.0;
  r21 = 0.0; r22 = 1.0;
  sumSAThreshold = 0;
  calculationAlgorithm = DOS_CALC;

}

// --------------------------------------------------------------------------------------

void PosCalculation::ComputePosition(Int32Buffer &sa, BP &p) {

  p.va = (double)(sa[0][0] + sa[1][0]) / 2.0;
  p.vb = (double)(sa[0][1] + sa[1][1]) / 2.0;
  p.vc = (double)(sa[0][2] + sa[1][2]) / 2.0;
  p.vd = (double)(sa[0][3] + sa[1][3]) / 2.0;
  ComputePosition(p);

  double c_to_a = M_PI * ((double)(sa[0][10] + sa[1][10]) / 2.0) / 2147483648.0;
  double c_to_b = M_PI * ((double)(sa[0][11] + sa[1][11]) / 2.0) / 2147483648.0;
  double cangle = M_PI * ((double)(sa[0][12] + sa[1][12]) / 2.0) / 2147483648.0;
  double c_to_d = M_PI * ((double)(sa[0][13] + sa[1][13]) / 2.0) / 2147483648.0;

  p.aangle = cangle - c_to_a;
  p.bangle = cangle - c_to_b;
  p.cangle = cangle;
  p.dangle = cangle - c_to_d;

  p.vphase = -c_to_d;
  p.srphase = c_to_d/2.0 - c_to_b;
  p.syphase = c_to_d/2.0 - c_to_a;

  // We add 360 to be sure it's never negative.
  p.vphase = 360 + p.vphase * 180. / M_PI;
  p.srphase = p.srphase * 180. / M_PI;
  p.syphase = p.syphase * 180. / M_PI;

}

void PosCalculation::ComputeDDPosition(BP &p) {

  ComputePosition(p);

}

// --------------------------------------------------------------------------------------
#define EVAL_POLY(poly)                             \
sum = 0.0;                                          \
for (size_t j(0); j < poly.size(); j++)             \
sum += px[poly[j].nX] * py[poly[j].nY] * poly[j].c;

void PosCalculation::ComputePosition(BP &p) {

  // Inputs are in nm
  double KH = ds->kH;
  double KV = ds->kV;
  double KQ = (ds->kH + ds->kV) / 2.0;
  double OH = (ds->attr_HOffset_read[0] + ds->attr_HOffsetFine_read[0])*1e9;
  double OV = (ds->attr_VOffset_read[0] + ds->attr_VOffsetFine_read[0])*1e9;

  p.va *= kA;
  p.vb *= kB;
  p.vc *= kC;
  p.vd *= kD;

  double a =  p.va;
  double b =  p.vb;
  double c =  p.vc;
  double d =  p.vd;

  p.sum = a + b + c + d;

  if (p.sum < sumSAThreshold) {
    p.h = NAN;
    p.v = NAN;
    p.q = NAN;
    return;
  }

  switch( calculationAlgorithm ) {

    // Delta Over Sigma calculation
    case DOS_CALC: {

      // Pos in m
      p.h = (KH * (((a + d) - (b + c)) / p.sum) + OH)*1e-9;
      p.v = (KV * (((a + b) - (c + d)) / p.sum) + OV)*1e-9;
      p.q = (KQ * (((a + c) - (b + d)) / p.sum) )*1e-9;

    }
    break;

    // Compute position according to polynomial transform
    case POLYNOMIAL_CALC: {

      double iS = 1.0 / (a + b + c + d);
      double X = (a - b - c + d) * iS;
      double Y = (a + b - c - d) * iS;
      double q = (a - b + c - d) * iS;
      double sum;

      double powx = 1.0;
      double powy = 1.0;
      for (int j(0); j <= maxXPow; j++) {
        px[j] = powx;
        powx *= X;
      }
      for (int j(0); j <= maxYPow; j++) {
        py[j] = powy;
        powy *= Y;
      }

      EVAL_POLY(hPoly);
      p.h = (1e6*sum + OH)*1e-9;

      EVAL_POLY(vPoly);
      p.v = (1e6*sum + OV)*1e-9;

      // For S and Q, we need coordinates in mm
      X = p.h * 1e3;
      Y = p.v * 1e3;
      powx = 1.0;
      powy = 1.0;
      for (int j(0); j <= maxXPow; j++) {
        px[j] = powx;
        powx *= X;
      }
      for (int j(0); j <= maxYPow; j++) {
        py[j] = powy;
        powy *= Y;
      }

      EVAL_POLY(qPoly);
      p.q = (KQ*(q-sum))*1e-9;

      EVAL_POLY(sPoly);
      p.sum = p.sum / sum;

    }
    break;

  }

  // Tilt rotation
  double rh = r11 * p.h + r12 * p.v;
  double rv = r21 * p.h + r22 * p.v;
  p.h = rh;
  p.v = rv;

}

} // LiberaSparkSRBPM_ns
