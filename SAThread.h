//+=============================================================================
//
// file :         SAThread.h
//
// description :  Include for the ConnectionThread class.
//                This class is used for reading SA stream
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERASPARKSRBPM_SATHREAD_H
#define LIBERASPARKSRBPM_SATHREAD_H

#include <LiberaSparkSRBPM.h>
#include <vector>
#include <mutex>

namespace LiberaSparkSRBPM_ns {


class SAThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    SAThread(LiberaSparkSRBPM *, PosCalculation *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    bool isConnected;

    void setHistoryLength(int length);
    int  getHistoryLength();
    void setAveraging(bool avg);
    bool isAveraging();
    void setAvgLength(int length);
    int  getAvgLength();
    void setStatLength(int length);
    int  getStatLength();

    string getStatus() {
      omni_mutex_lock l(mutex);
      return threadStatus;
    }

    double GetVa();
    double GetVb();
    double GetVc();
    double GetVd();
    double GetSum();
    double GetX();
    double GetZ();
    double GetXStdDev();
    double GetZStdDev();
    double GetQ();
    double GetAAngle();
    double GetBAngle();
    double GetCAngle();
    double GetDAngle();
    double GetVerifPhase();
    double GetSRPhase();
    double GetSYPhase();

    void getVaHistory(double *hist,int *length);
    void getVbHistory(double *hist,int *length);
    void getVcHistory(double *hist,int *length);
    void getVdHistory(double *hist,int *length);
    void getSumHistory(double *hist,int *length);
    void getXHistory(double *hist,int *length);
    void getZHistory(double *hist,int *length);
    void getQHistory(double *hist,int *length);
    void getVerifPhaseHistory(double *hist,int *length);
    void getSRPhaseHistory(double *hist,int *length);
    void getSYPhaseHistory(double *hist,int *length);

    void wakeup();

private:

    void computeAveraging();
    void computeStats();

    LiberaSparkSRBPM *ds;
    omni_mutex &mutex;
    PosCalculation *PC;
    vector<BP> saValues;
    int historyLength;
    int avgLength;
    int statLength;
    bool average;
    double HStdDev;
    double VStdDev;
    int64_t saCount;

    BP currentPos;

    std::shared_ptr<RStream>      m_stream;
    std::shared_ptr<StreamClient> m_streamClient;
    std::shared_ptr<Int32Buffer> m_buf;

    bool OpenStream();

    string threadStatus;

}; // class SAThread

} // namespace LiberaSparkSRBPM_ns

#endif //LIBERASPARKSRBPM_SATHREAD_H
