//+=============================================================================
//
// file :         IQThread.h
//
// description :  Include for the IQThread class.
//                This class is used for reading DD IQ data (TBT)
//
// project :      LiberaSparkSRBPM TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERASPARKSRBPM_IQTHREAD_H
#define LIBERASPARKSRBPM_IQTHREAD_H

#include <LiberaSparkSRBPM.h>

namespace LiberaSparkSRBPM_ns {

class IQThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    IQThread(LiberaSparkSRBPM *,omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

private:

    vector<BP> iqValues;
    LiberaSparkSRBPM *ds;
    omni_mutex &mutex;

    std::shared_ptr<RSource>      m_dod;
    std::shared_ptr<DodClient>    m_dodClient;
    std::shared_ptr<Int32Buffer>  m_buf;

    bool OpenDod();
    void computePosition(std::shared_ptr<Int32Buffer> dd,size_t nb);

public:

    double *getIa(int *length);
    double *getIb(int *length);
    double *getIc(int *length);
    double *getId(int *length);
    double *getQa(int *length);
    double *getQb(int *length);
    double *getQc(int *length);
    double *getQd(int *length);
    bool    hasData();

    void wakeup();

}; // class IQThread

} // namespace LiberaSparkSRBPM_ns

#endif //LIBERASPARKSRBPM_IQTHREAD_H
